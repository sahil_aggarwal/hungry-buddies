var controlPanelViewModel;
function ControlPanelViewModel()
{
	var self = this;
	this.vendor = ko.observable({});
	this.items = ko.observable({});
	this.item = ko.observable({});
	this.displayVendorForm = ko.observable(false);
	this.displayItemForm = ko.observable(false);
	this.hour = ko.observableArray(["1","2","3","4","5","6","7","8","9","10","11","12"]);
	this.minutes = ko.observableArray(["10","20","30","40","50"]);
	this.ampm = ko.observableArray(["AM","PM"]);
	this.startTime = new Time();
	this.closingTime = new Time(); 
        this.vendorsArray = ko.observableArray();
        this.cuisines = ko.observableArray();
        this.message = ko.observable();
        this.feedbacks = ko.observableArray();
	this.admin = ko.observable({});
	this.types = ko.observableArray([{type : 'Veg'},{type : 'Non-Veg'}]);
	this.packings = ko.observableArray([{packing : 'Polythene'},{packing : 'Box'}]);
    this 
	 
	this.showUsers = function()
	{
		$.get('/users',function(data)
		{
			
		});
	}	
	
        /*login the admin*/ 
	this.adminLogin = function()
	{
		$.get('/adminPage',self.admin(),function(returnedData){
			if(returnedData.error)
			{
				 //document.getElementById("message").style.display = 'block';
				//self.message(returnedData.error);
				setTimeout(function(){window.location.href='/';},1200);
			}
			else if(returnedData.message == "Success")
			{
				window.location.href='/showAdmin';
			}


		});
	}


        this.deleteFeedback = function(feedback)
	{
		$.get('/deleteFeedback',{id: feedback.id},function(data){
			if(data.message == "Success")
			{
				self.feedbacks.remove(feedback);
			}
			else
			{
				 document.getElementById("message").style.display = 'block';
                                 self.message(data.error);
                                 setTimeout( function() {$("#message").fadeOut(1000);}, 1000)
			}
		});
	}	
	this.getFeedbacks = function()
	{
		$.get('/feedback',function(data){
			data = JSON.parse(data);
				var feedback = {};
				var feedbacks =$.map(data , function(item){							
							feedback.uname = item.uname;
							feedback.email = item.email;
							feedback.umessage = item.umessage;
							feedback.id = item.id;
							feedback.time = item.time;
							return new FeedbackModel(feedback);
								
						});
				if(feedbacks.length != 0)
				{
					self.feedbacks(feedbacks);
				}	
				else
				{
					document.getElementById("message").style.display = 'block';
                                        self.message("No feedbacks present");
                                        setTimeout( function() {$("#message").fadeOut(1000);}, 1000)
				}
		});
	}
        this.getCuisines = function()
	{

		$.get('/getCuisines',
			function(data)
			{
				data = JSON.parse(data);
				var cuisine = {};
				var cuisines =$.map(data , function(item){							
							cuisine.cuisine_name = item.cuisine_name;
							return new CuisineModel(cuisine);
								
						});
				self.cuisines(cuisines);	
			});
        }
	this.orders = ko.observableArray();
	this.orderItems = ko.observableArray();
	
	/*delete the order*/
	this.deleteOrder = function(order)
	{
		$.get('/deleteOrder',{id : order.id},function(data){
			if(data.message == "Success")
			{
				self.orders.remove(order);
			}
			else
			{
				 document.getElementById("message").style.display = 'block';
                                 self.message(data.error);
                                 setTimeout( function() {$("#message").fadeOut(1000);}, 1000)
			}
			
		});
	}
		
	this.getOrders = function()
	{
		$.get('/getOrders',
                     function(data){
                     data = JSON.parse(data);
                     var orders = $.map(data,function(item)
                                       {
                                                   return new Order(item);
                                       });
			if(orders.length == 0)
			{
				 document.getElementById("message").style.display = 'block';
                                 self.message("No Orders are present right now");
                                 setTimeout( function() {$("#message").fadeOut(1000);}, 1000)
			}
			else
			{
				self.orders(orders);
			}
		});
	}	
	this.getOrderItems = function(order)
	{
		var order = JSON.stringify(order);
		$.post('/orderItems',order,function(returnedData){
			self.orderItems.removeAll();
			self.orderItems(JSON.parse(returnedData));	
		});
	}

	this.itemForm = function(id)
	{
		self.displayVendorForm(false);
                self.item().vendor_id = id;
		self.displayItemForm(true);
                
	}

	this.addItem = function()
	{
		self.displayItemForm(false);
		var item = JSON.stringify(self.item());
		$.post('/addItem',item,function(returnedData){
			self.item({});	
		});
	}	
	
	this.deleteItem = function(item)
	{
		var item = JSON.stringify(item);
		$.post('/deleteItem',item,function(returnedData){
			self.items(JSON.parse(returnedData));
		});
	}

	this.showItem = function(vendor)
	{
		var vendor = JSON.stringify(vendor);
                self.orders.removeAll();
                self.orderItems.removeAll();
		$.post('/showItem',vendor,function(returnedData){
			//self.items.removeAll();
			var items = JSON.parse(returnedData);
			if(items.length == 0)
			{
				document.getElementById("message").style.display = 'block';
                                self.message("No Item is present for this vendor");
                                setTimeout( function() {$("#message").fadeOut(1000);}, 1000)
			}
			else
			{
				self.items(items);
			}
		});
	}
		
	this.deleteVendor = function(vendor)
	{
		vendor = JSON.stringify(vendor);
		$.post('/deleteVendor',vendor,function(returnedData){});
		self.vendorsArray.remove(this);
	}
		
	this.printVendors = function()
	{
		$.get("/getVendorDetails",function(data){
			
			self.vendorsArray.removeAll();
                      
                        data = JSON.parse(data);
                        var vendors = $.map(data, 
                                            function(item)
                                            {
                                                var vendor = item;
						vendor.delivery_from = changeTimeFormat(item.delivery_from);
                                                vendor.delivery_to = changeTimeFormat(item.delivery_to);
                                                return vendor;

                                            });
			self.vendorsArray(vendors);
		});
	}
	this.vendorForm = function()
	{
		this.displayItemForm(false);
		this.displayVendorForm(true);
		self.vendor({});
	}
	
	this.addVendor = function()
	{
		this.displayVendorForm(false);
              
                if(this.startTime.ampm() == "PM" && this.startTime.hour() < 12)
		{
				var startHour = parseInt(this.startTime.hour())+12;
		}
		else if(this.startTime.ampm() == "AM" && this.startTime.hour() == 12)
		{
				var startHour=parseInt(this.startTime.hour())-12;
		}
                else
                {
			var startHour = this.startTime.hour();
                }
                if(this.closingTime.ampm() == "PM" && this.closingTime.hour() < 12)
		{
				var endHour = parseInt(this.closingTime.hour())+12;
		}
		else if(this.closingTime.ampm() == "AM" && this.closingTime.hour() == 12)
		{
				var endHour=parseInt(this.closingTime.hour())-12;
		}
                else
                {
			var endHour = this.closingTime.hour();
                }
		this.vendor().delivery_from = "2014-02-01 "+startHour+":"+this.startTime.minutes();
		this.vendor().delivery_to = "2014-02-01 "+endHour+":"+this.closingTime.minutes();
		var vendor = JSON.stringify(this.vendor());
		$.post("/vendor",
                       vendor,
                       function(data)
                       {
				self.vendor().delivery_from = changeTimeFormat(self.vendor().delivery_from);
                                self.vendor().delivery_to = changeTimeFormat(self.vendor().delivery_to);	
				self.vendor().id = data.id;
				self.vendorsArray.push(self.vendor());
                 
		       });
		
	}
	

	this.init = function()
	{
		this.printVendors();

                this.getCuisines();


	}
}
function changeTimeFormat(time)
{
         var date = new Date(time);
	 var hours = date.getHours() == 0 ? "12" : date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
         var minutes = (date.getMinutes() < 9 ? "0" : "") + date.getMinutes();
         var ampm = date.getHours() < 12 ? "AM" : "PM";
         var formattedTime = hours + ":" + minutes + " " + ampm;
         return formattedTime;
}


$().ready(function(){

	controlPanelViewModel = new ControlPanelViewModel();
	if ($('#controlPanel').get(0)) {
            ko.applyBindings(controlPanelViewModel, $('#controlPanel').get(0));
         }
	if ($('#adminLoginModal').get(0)) {
            ko.applyBindings(controlPanelViewModel, $('#adminLoginModal').get(0));
         }	
          $("#adminLoginModal").modal('show');
        controlPanelViewModel.init();
});
