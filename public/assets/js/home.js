ko.bindingHandlers.jqSlider = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        //initialize the control
        var options = allBindingsAccessor().jqOptions || {};
        $(element).slider(options);


        //handle the value changing in the UI
        ko.utils.registerEventHandler(element, "slidechange", function() {
            //would need to do some more work here, if you want to bind against non-observables
            var observable = valueAccessor();

            observable($(element).slider("value"));
        });

    },
    //handle the model value changing
    update: function(element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).slider("value", value);   

    }

     	
};

var homeViewModel;
function HomeViewModel()
{
	var self = this;
        this.cuisines = ko.observableArray();
	this.selectedCuisine = ko.observable();
        this.hours = ko.observableArray();
	this.minutes = ko.observableArray();
	this.time = ko.observableArray();
        this.selectedHour = ko.observable();
	this.selectedMinute = ko.observable();
	this.selectedClock = ko.observable();
	this.vendors = ko.observableArray();
	this.vendorstemp = ko.observableArray();
	this.selected_city = ko.observable("All");
	this.selectedVendors = ko.observableArray();
	this.city_array = ko.observableArray(["All","Delhi"]);
	this.cuisines = ko.observableArray();
	this.filteredItems = ko.observableArray();
	this.allItems = ko.observableArray();
	this.checkedCuisine = ko.observableArray();
	this.priceLimit = ko.observable(50);
	this.foodType = ko.observableArray();
	
	this.getVendors = function()
	{
		
			
				self.vendors([{"vendor_name":"Bikanervala","vendor_id":1,"vendor_house_number":"","vendor_street":"Sector 63","vendor_city":"Noida","item":"Mini Thali(2 Roti,Panner Veg,Dal Makhani,Raita,Pic","item_id":1,"price":135,"type":"Veg","packing":"Thali","cuisine":"North Indian"},{"vendor_name":"Bikanervala","vendor_id":1,"vendor_house_number":"","vendor_street":"Sector 63","vendor_city":"Noida","item":"Mini Thali(2 Roti,Panner Veg,Dal Makhani,Raita,Pic","item_id":1,"price":135,"type":"Veg","packing":"Thali","cuisine":"Thali"},{"vendor_name":"Meeting Point Corner","vendor_id":2,"vendor_house_number":"","vendor_street":"Sector 62","vendor_city":"Noida","item":"Special Thali(2 Roti,Panner Veg,Dal Makhani,Raita,","item_id":2,"price":135,"type":"Veg","packing":"Thali","cuisine":"North Indian"},{"vendor_name":"Meeting Point Corner","vendor_id":2,"vendor_house_number":"","vendor_street":"Sector 62","vendor_city":"Noida","item":"Special Thali(2 Roti,Panner Veg,Dal Makhani,Raita,","item_id":2,"price":135,"type":"Veg","packing":"Thali","cuisine":"Thali"},{"vendor_name":"Essen Foods","vendor_id":4,"vendor_house_number":"","vendor_street":"Sector 65","vendor_city":"Noida","item":"Special Thali(2 Roti,Panner Veg,Dal Makhani,Raita,","item_id":3,"price":175,"type":"Veg","packing":"Thali","cuisine":"North Indian"},{"vendor_name":"Essen Foods","vendor_id":4,"vendor_house_number":"","vendor_street":"Sector 65","vendor_city":"Noida","item":"Special Thali(2 Roti,Panner Veg,Dal Makhani,Raita,","item_id":3,"price":175,"type":"Veg","packing":"Thali","cuisine":"Thali"}]);
				self.vendors.sort(compare);	
				self.changeDataFormat();
			
	}
	
	this.changeDataFormat = function()
	{
		var temp_vendor = {};
		temp_vendor.items = [];
		temp_vendor.vendor_cuisines = [];
		var vendor_id=0;		
		for(var i = 0; i< this.vendors().length; i++)
		{
			var vendor = this.vendors()[i];
			if(vendor_id == this.vendors()[i].vendor_id)
			{
				temp_vendor.items.push({name:vendor.item,packing:vendor.packing,price:vendor.price,id:vendor.item_id,cuisine:vendor.cuisine,type:vendor.type});	
			}
			else
			{
				if(vendor_id!=0)
				{
					var temp = new VendorModel(temp_vendor);
					self.vendorstemp.push(temp);					
				}
				vendor_id = vendor.vendor_id;
				temp_vendor={};
				temp_vendor.items = [];
				temp_vendor.vendor_cuisines = [];
				temp_vendor.vendor_id = vendor.vendor_id;
				temp_vendor.vendor_name = vendor.vendor_name;
				temp_vendor.vendor_city = vendor.vendor_city;
				temp_vendor.vendor_street = vendor.vendor_street;
				temp_vendor.vendor_house_number = vendor.vendor_house_number;
				temp_vendor.vendor_delivery_start = '';
				temp_vendor.vendor_delivery_end = '';
				temp_vendor.items.push({name:vendor.item,packing:vendor.packing,price:vendor.price,id:vendor.item_id,cuisine:vendor.cuisine,type:vendor.type});
						
			}
			if(temp_vendor.vendor_cuisines.length ==0 || !temp_vendor.vendor_cuisines.indexOf(vendor.cuisine))				
			{
				temp_vendor.vendor_cuisines.push({cuisine:vendor.cuisine});
			}		
		}
	}
	
	this.vendorSelected = function()
	{
		for(var i = 0;i < self.vendorstemp().length; i++)
		{
			if(self.selected_city() == "All" || self.selected_city() == self.vendorstemp()[i].vendor_city)			
			{
				self.selectedVendors.push(self.vendorstemp()[i]);
			}
		}
	}

	this.getCities = function()
	{
		var temp;
		for(i = 0; i < this.vendorstemp().length; i++)
		{
			temp = this.vendorstemp()[i].vendor_city;

			if(this.city_array.indexOf(temp) == -1)
			{			
				this.city_array.push(temp);
			}
		}
	}
	
	this.getCuisines = function()
	{
		$.get('/getCuisines',
			function(data)
			{
				data = JSON.parse(data);
				var cuisine = {};
				var cuisines =$.map(data , function(item){							
							cuisine.cuisine_name = item.cuisine_name;
							return new CuisineModel(cuisine);
								
						});
				self.cuisines(cuisines);	
			});
	}
	
	this.printMenu = function(vendor)
	{
		self.filteredItems.removeAll();
		self.allItems.removeAll();
		for(var i =0; i<vendor.items.length; i++)
		{		
									
			self.filteredItems.push({cuisine: vendor.items[i].cuisine, name: vendor.items[i].name, packing: vendor.items[i].packing, price: vendor.items[i].price, type: vendor.items[i].type});
			self.allItems.push({cuisine: vendor.items[i].cuisine, name: vendor.items[i].name, packing: vendor.items[i].packing, price: vendor.items[i].price, type: vendor.items[i].type, quantity: 0});
	
		}
	}
	
	this.refineSearch = function()
	{
			
		var temp_var;
	
		if(self.filteredItems().length != 0)
		{
			self.filteredItems.removeAll();
		}

		if(self.checkedCuisine().length == 0)
		{
			for(var i = 0; i< self.allItems().length; i++)
			{			
				self.filteredItems.push({cuisine: self.allItems()[i].cuisine, name: self.allItems()[i].name, packing: self.allItems()[i].packing, price: self.allItems()[i].price, type: self.allItems()[i].type});
			}
		}
				
		for(var i = 0; i< self.checkedCuisine().length; i++)
		{
		
			temp_var = self.checkedCuisine()[i];
			for(var j = 0; j< self.allItems().length; j++)
			{
				if(temp_var == self.allItems()[j].cuisine)
				{
					if(self.filteredItems.indexOf(self.allItems()[j])==-1 && self.allItems()[j].price <= self.priceLimit)
					{	
						
						if(foodType().length == 0 || foodType().length == 2 || foodType.indexOf(self.allItems()[j].type) !=-1)			
						{
							self.filteredItems.push({cuisine: self.allItems()[j].cuisine, name: self.allItems()[j].name, packing: self.allItems()[j].packing, price: self.allItems()[j].price, type: self.allItems()[j].type});
						}
					}
				}
			}
		}
		return true;
	}	
	

	this.init = function()
	{
                this.getVendors(); 
		this.getCities();
		this.getCuisines();
		/*for(var i =1  ; i <= 9 ; i++)
		{
			this.hours.push({hour : "0"+i});
		}
		for(var i = 10 ; i< 60 ; i+=10)
		{
			this.minutes.push({minute : i});	
		}
		for(var i = 10 ; i<= 12 ; i++)
		{
			this.hours.push({hour : i});
		}
                this.time.push({time : "AM" });
		this.time.push({time : "PM"});*/	
	}
 
        this.order = function() 
	{
		//alert(this.selectedCuisine()+this.selectedHour()+this.selectedMinute()+this.selectedClock());
              
	}
}

function compare( a , b )
{
    if(a.vendor_id < b.vendor_id)
    {
        return 1;
    }
    if(a.vendor_id > b.vendor_id)
    {
        return -1;
    }
    return 0;
}


$().ready(function(){

	homeViewModel = new HomeViewModel();
	ko.applyBindings(homeViewModel);
        homeViewModel.init();
});
