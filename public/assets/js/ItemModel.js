function ItemModel(data,quantity)
{
	this.id = data.id;
	this.price = data.price;
	this.name = data.name;
	this.type = data.type;
	this.packing = data.packing;
	this.vendor_id = data.vendor_id;
	this.cuisine = data.cuisine;
	this.quantity = ko.observable(quantity);
        //this.quantity = ko.observable(1);
	this.totalPrice = ko.computed(function(){
		var x = this.quantity() * this.price;	
		return x;
				
	},this);
	this.increment = function(){
		this.quantity(parseFloat(this.quantity()) + 1);
		var x;
	}
	
	this.decrement = function(){
		if(this.quantity() > 1)
		{
		 	this.quantity(parseFloat(this.quantity()) - 1);
                        return;
		}
		if(this.quantity() == 1)
		{
			vendorViewModel.orders.remove(this);
		}
                	
	}
	this.remove = function(){
		vendorViewModel.orders.remove(this);
	}
	this.addMultiple = function(item){
		
		var flag = 0;
		
		for(var i = 0; i< vendorViewModel.orders().length; i++)
		{
			if(this.id == vendorViewModel.orders()[i].id)
			{
				vendorViewModel.orders()[i].quantity(parseFloat(vendorViewModel.orders()[i].quantity()) + parseFloat(this.quantity())); 
				return;
			}
		}
			
                var temp = new ItemModel(this,this.quantity());			 			
		vendorViewModel.orders.push(temp);
		

	}

}

function VendorModel(data)
{
	this.vendor_name = data.vendor_name;
	this.vendor_id = data.vendor_id;
	this.vendor_house_number = data.vendor_house_number;
	this.vendor_street = data.vendor_street;
	this.vendor_city = data.vendor_city;
        this.delivery_start = data.delivery_start;
        this.delivery_end = data.delivery_end;
        this.items = data.items;
	this.vendor_cuisines = data.vendor_cuisines;
        this.status = data.status;
	this.vendorCuisine = ko.computed(function() {

		var cuisine;
		if(this.vendor_cuisines[0].cuisine)
		{
			cuisine = this.vendor_cuisines[0].cuisine;
		}
        	for(var i = 1 ;i < this.vendor_cuisines.length;i++)
		{
			cuisine = cuisine+','+this.vendor_cuisines[i].cuisine;
		}
		return cuisine;
   	 }, this);

	this.printMenu = function(vendor)
	{
		vendorViewModel.filteredItems.removeAll();
		vendorViewModel.allItems.removeAll();
		vendorViewModel.selectedVendor(vendor);
                vendorViewModel.showMenu(true);
                vendorViewModel.showVendor(false);
		for(var i =0; i< this.items.length; i++)
		{		
			var itemTemp = new ItemModel({cuisine: this.items[i].cuisine, name: this.items[i].name, packing: this.items[i].packing, price: this.items[i].price, type: this.items[i].type, id: this.items[i].id, vendor_id: this.items[i].vendor_id},1);						
                        //same item with diff cuisines is listed twice ..have to correct it
			vendorViewModel.filteredItems.push(itemTemp);
			vendorViewModel.allItems.push(itemTemp);
	
		}
 
                //can modify getCuisines to list cuisines for a vendor..current method gets all cuisines
                vendorViewModel.getCuisines();
	}
}

function CuisineModel(data)
{
	this.cuisine_name = ko.observable(data.cuisine_name);
	
}

function Order(data)
{
	this.user = data.user;
	this.address = data.address;
        this.id = data.id;
        this.status = ko.observable(data.status);
        this.time = data.time;
        this.items = data.items;
        this.username = data.username;
        this.contactNum = data.contactNum;
        this.setOrderStatus = function()
        {
	    var self = this;
            $.post('/setOrderStatus',
                    {id : this.id},
                    function(returnedData)
                    {
                        self.status(true);
                    }
		  );            
        }
}

function Time()
{
	this.hour = ko.observable();
	this.minutes = ko.observable();
	this.ampm = ko.observable();
	this.format = "12hr";
	this._12hrto24hr = function()
	{
		if(this.format == "12hr")
		{
			if(this.ampm() == "PM" && this.hour() < 12)
			{
				this.hour(parseInt(this.hour())+12);
			}
			if(this.ampm() == "AM" && this.hour() == 12)
			{
				this.hour(parseInt(this.hour())-12);
			}
			this.format = "24hr";
		}
	}
	
}
/*
function send() {
    var link = 'mailto:aggarwalsahil1992@gmail.com?subject=Message from aggarwalsahil1992@gmail.com&body=Its working';
             //+document.getElementById('email').value
             //+'&body='+document.getElementById('email').value;
    window.location.href = link;
}
*/
function FeedbackForm()
{
	this.name = ko.observable();
	this.comment = ko.observable();
	this.sendFeedback = function(feedback)
	{
		$.post('/contact',this,function(returnedData){});
	}
}

function User(data)
{
	this.name = data.name;
	this.password = data.password
	this.emailId = data.emailId;
	this.contactNum = data.contactNum;
	this.address = data.address;
	this.createUser = function()
	{
		var user = JSON.stringify(this);
		$.post('/user',user,
                      function(data){
			if(data.error)
			{
				document.getElementById("signupmessage").style.display = 'block';	
				headerViewModel.message(data.error);
				setTimeout( function() {$("#signupmessage").fadeOut(1000);}, 1000);
			}
			else
			{
				document.getElementById("signupmodal").style.display = 'none';
				$("#signup").modal('hide');
		                $("#onsignup").modal('show');
			}	
		});
	}
}

function FeedbackModel(data)
{
	this.uname = data.uname;
	this.id = data.id;
	this.email = data.email;
	this.umessage = data.umessage;
	this.time = data.time;
}
