var vendorViewModel;

function VendorViewModel()
{
	var self = this;
        this.vendors = ko.observableArray();
        this.vendorLocations = ko.observableArray();
        this.selectedLocation = ko.observable();
	this.selectedVendors = ko.observableArray();

	
	
	this.getVendors = function()
	{
		
			
				$.get('/allVendor',
                                       function(data)
					{
						data = JSON.parse(data);
						self.vendors(data);
                                                self.getVendorLocations();					
						//self.vendors([{"vendor_name":"Anu Sweets","vendor_id":1,"start_time":"2014-02-09T10:00:00+05:30","end_time":"2014-08-09T22:00:00+05:30","vendor_house_number":"12","street":"Jawahar Nagar","vendor_city":"SGNR","item":"Rasgulla","item_id":1,"price":150,"type":"Veg","packing":"Polythene","cuisine":"Sweet"}]);

						
					});
					
			
	}
	
        
	
	this.getVendorLocations = function() 
	{
		for(var i =0 ; i < this.vendors().length ; i++)
		{
			if(this.vendorLocations.indexOf(this.vendors()[i].street)==-1)
			{
				this.vendorLocations.push({vendor_street : this.vendors()[i].street});
			}
		}
	}

	this.findRestaurants = function()
	{
               
		var url = '/vendor/'+this.selectedLocation();
                $.get(url,
                      function(data)
                      {
                             window.location.href='/vendor';
                           
                       });
                /*document.getElementById("step1").style.opacity = 0.7;
                document.getElementById("step2").style.opacity = 0;
		for(var i = 0 ; i < self.vendorstemp().length ; i++)
		{	
			if(self.vendorstemp()[i].vendor_street == this.selectedLocation())
			{
				self.selectedVendors.push(self.vendorstemp()[i]);
			}
		}*/
                
	}
	
	
	this.init = function()
	{	

                //document.getElementById("step1").style.opacity=0;

		this.getVendors();
	}
}


$().ready(function(){

	vendorViewModel = new VendorViewModel();
        if ($('#index').get(0)) {
            ko.applyBindings(vendorViewModel, $('#index').get(0));
         }
        $("#popupModal").modal('show');
        vendorViewModel.init();
});
