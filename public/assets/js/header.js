var headerViewModel;
function HeaderViewModel()
{
	self = this;
	this.user = ko.observable({});
        this.uname = ko.observable();
	this.password = ko.observable();
        this.message = ko.observable();
        this.userOrders = ko.observableArray();
	this.userProfile = ko.observable({});
        this.templateToRender = ko.observable("itemTmpl");
	this.temp;
	this.feedback = ko.observable({});

	this.closeSignUp = function()
	{
		$("#onsignup").modal('hide');
		$(".modal-backdrop").remove();
	}
	this.addFeedback = function()
	{
		$.post('/addFeedback',this.feedback(),function(data){
			if(data.message == "Success")
			{
				$("#contactForm").html("Thanks for your feedback");	
			}
		});
	}
	this.viewProfile = function()
	{
                var self = this;
                $.get('/profile',
                       function(data)
			{
				if(data.error)
				{
                                        document.getElementById("message").style.display = 'block';
                                        self.message(data.error);
                                        setTimeout( function() {$("#message").fadeOut(1000);}, 1000)
				}
                                else
					window.location.href='/viewProfile';
			});
				
	}

	this.getProfile = function()
	{
		$.get('/getProfile',function(data){
			if(data.error)
			{
				//document.getElementById("message").style.display = 'block';
                                //self.message(data.error);
                                //setTimeout( function() {$("#message").fadeOut(1000);}, 1000)
			}
			else
			{	
				
				self.userProfile(data);
			}
		});
	};
	
	this.editClicked = function()
	{
		self.temp = JSON.stringify(self.userProfile());
		self.templateToRender("editTmpl");    		
	}
	
	this.acceptClicked = function()
	{
	      this.templateToRender("itemTmpl"); 
	      $.post('/updateProfile',self.userProfile(),function(data){
				if(data.error) 
				{     	 		
					document.getElementById("message").style.display = 'block';
                              	  	self.message(data.error);
                               		setTimeout( function() {$("#message").fadeOut(1000);}, 1000)
				}
	      });
	}
	
	this.cancelClicked = function()
	{
		self.userProfile(JSON.parse(self.temp));
		this.templateToRender("itemTmpl");
	}
	
	this.templateToUse = function()
	{
		return self.templateToRender();
	}

	this.createUser = function()
	{
		var user = new User(this.user());
		user.createUser();	
	}
        this.login=function()
  	{
                  var self = this;
    		$.post("/login",
    		  	{user_name : this.uname(), pass : this.password()},
    		  	function(data)
    		  	{
                            document.getElementById("message").style.display = 'block';
			    if(data.error){
                                
    				self.message(data.error.message);
 			    }
                            else{
				
                                
                                document.getElementById("message").style.display = 'block';
                                self.message("You have logged in successfully !!!");
				setTimeout(function(){window.location.reload();},3000);
                                self.showNav();
			    }
                            setTimeout( function() {$("#message").fadeOut(1000);}, 1000);
    			 
    			}
    		  
    	 		);
                        
    
  	};

        this.viewCart = function()
	{
                var self = this;
                $.get('/cart',
                       function(data)
			{
				if(data.error)
				{
                                        document.getElementById("message").style.display = 'block';
                                        self.message(data.error);
                                        setTimeout( function() {$("#message").fadeOut(1000);}, 1000)
				}
                                else
					window.location.href='/showcart';
			});
				
	}
	this.viewCartHelper = function(){
		var self=this;
		$.get('/getcart',
			function(data)
			{
				
					//window.location.href = '/cart';
					data = JSON.parse(data);
					var userOrders = $.map(data, function(item)
								     {
									if(item.username == $("#user").val())
									{
										return item;
									}
								      }
								);
					self.userOrders(userOrders);	
                                        
				
			});
	}
	


        this.showNav = function()
	{
		if($("#user").val() ==''||$("#user").val()==null)
		{
			document.getElementById("signup").style.display = 'inline';
			document.getElementById("signin").style.display = 'inline';
                        document.getElementById("signout").style.display = 'none';
		}
		else
		{
			document.getElementById("signup").style.display = 'none';
			document.getElementById("signin").style.display = 'none';
                        document.getElementById("signout").style.display = 'inline';
		}
	};

}
$().ready(function(){

	$("ul.content").hide();
	$(".heading").click(function(){$(this).next("li").children("ul.content").slideToggle("slow");});

	headerViewModel = new HeaderViewModel();
         if ($('#header').get(0)) {
            ko.applyBindings(headerViewModel, $('#header').get(0));
         }
         if ($('#message').get(0)) {
            ko.applyBindings(headerViewModel, $('#message').get(0));
         }
         if ($('#cart').get(0)) {
            ko.applyBindings(headerViewModel, $('#cart').get(0));
         }
	 if ($('#profile').get(0)) {
            ko.applyBindings(headerViewModel, $('#profile').get(0));
         }
	  if ($('#contactForm').get(0)) {
            ko.applyBindings(headerViewModel, $('#contactForm').get(0));
         }
	if ($('#goToHome').get(0)) {
            ko.applyBindings(headerViewModel, $('#help').get(0));
         }
	
        // headerViewModel.viewCartHelper();
	 headerViewModel.getProfile();
         headerViewModel.showNav();
});
