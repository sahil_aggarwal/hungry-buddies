ko.bindingHandlers.jqSlider = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        //initialize the control
        var options = allBindingsAccessor().jqOptions || {};
        $(element).slider(options);


        //handle the value changing in the UI
        ko.utils.registerEventHandler(element, "slidechange", function() {
            //would need to do some more work here, if you want to bind against non-observables
            var observable = valueAccessor();

            observable($(element).slider("value"));
        });

    },
    //handle the model value changing
    update: function(element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).slider("value", value); 
        vendorViewModel.refineSearch();  

    }


};

var vendorViewModel;

function VendorViewModel()
{
	var self = this;
        this.vendors = ko.observableArray();
	this.vendorstemp = ko.observableArray();
	this.priceLimit = ko.observable(1500);
        this.foodType = ko.observableArray();
	this.cuisines = ko.observableArray();
       	this.checkedCuisine = ko.observableArray();  
	this.selectedVendors = ko.observableArray();
	this.allItems = ko.observableArray();
	this.filteredItems = ko.observableArray();
	this.orders = ko.observableArray();
	this.orderId = ko.observable();
	this.order = ko.observable({});
        this.order().contactNum = ko.observable();
        this.order().user = ko.observable();
        this.order().address = ko.observable();
	this.selectedVendor = ko.observableArray();
        this.showMenu = ko.observable(false);
        this.showDelivery = ko.observable(false);
        this.showThankyou = ko.observable(false);
        this.showVendor = ko.observable(false);
        this.signedIn = ko.observable(false);
        this.error = ko.observable();

	this.totalSum = ko.computed(function(){
		var temp = 0;
		for(var i = 0; i< self.orders().length; i++)
		{
			temp += self.orders()[i].totalPrice();
		}
		return temp;
	},this);

        self.getCuisines = function()
	{
		$.get('/getCuisines',
			function(data)
			{
				data = JSON.parse(data);
				var cuisine = {};
				var cuisines =$.map(data , function(item){							
							cuisine.cuisine_name = item.cuisine_name;
							return new CuisineModel(cuisine);
								
						});
				self.cuisines(cuisines);	
			});
	}

        this.refineSearch = function()
	{
			
		var temp_var, temp;
	
		if(self.filteredItems().length != 0)
		{
			self.filteredItems.removeAll();
		}

		if(self.checkedCuisine().length == 0 && self.foodType().length==0)
		{
			for(var i = 0; i< self.allItems().length; i++)
			{		
                                if(self.allItems()[i].price <= self.priceLimit())
				{	
					var itemTemp = new ItemModel(self.allItems()[i],1);
					self.filteredItems.push(itemTemp);
				}
			}
                        return true;
		}
				
		for(var i = 0; i< self.checkedCuisine().length; i++)
		{
		
			temp_var = self.checkedCuisine()[i];
			for(var j = 0; j< self.allItems().length; j++)
			{
				if(temp_var == self.allItems()[j].cuisine)
				{
					if(self.filteredItems.indexOf(self.allItems()[j])==-1 && self.allItems()[j].price <= self.priceLimit())
					{	
						
						if(self.foodType().length == 0 || self.foodType().length == 2 || self.foodType.indexOf(self.allItems()[j].type) !=-1)			
						{
							var itemTemp = new ItemModel(self.allItems()[j],1);
							self.filteredItems.push(itemTemp);
						}
					}
				}
			}

		}
                if(self.checkedCuisine().length == 0)
                {
			for(var i = 0; i< self.foodType().length; i++)
			{
		
				temp_var = self.foodType()[i];
				for(var j = 0; j< self.allItems().length; j++)
				{
					if(temp_var == self.allItems()[j].type)
					{
						if(self.filteredItems.indexOf(self.allItems()[j])==-1 && self.allItems()[j].price <= self.priceLimit())
						{	
						
							
								var itemTemp = new ItemModel(self.allItems()[j],1);
								self.filteredItems.push(itemTemp);
							
						}
					}
				}

			}
		}
		return true;

	}
        
	this.placeOrder = function()
	{
	       
               if(!self.orders().length)
		{
			self.error("You have no orders!!");
                        document.getElementById("error").style.display = "block";
                        setTimeout( function() {$("#error").fadeOut(3000);}, 1000);
                }
               else
               {
		       this.showMenu(false);
		       this.showDelivery(true);
		       document.getElementById("step3").style.opacity = 0;
		       document.getElementById("step2").style.opacity = 0.7;
               }
		this.autofillForm();
	};

        this.insertOrder = function(){
		
		this.signedIn(false);
		this.order().items = [];
		if($("#user").val()!='' && $("#user").val()!=null)
		{
                	this.order().username = $("#user").val();
		}
		else
		{
			this.order().username = 'Guest';
		}
		for(var i = 0; i< this.orders().length; i++)
		{
			this.order().items.push({item:self.orders()[i].id,quantity:self.orders()[i].quantity()});
		}
		$.post("/order",
                        {user : this.order().user(),username : this.order().username , address : this.order().address() ,items : this.order().items , contactNum : this.order().contactNum()},
                        function(returnedData)
                       {
			  if(returnedData.error)
			  {
				document.getElementById("message").style.display = 'block';
                                self.message(returnedData.error);
                                setTimeout( function() {$("#message").fadeOut(1000);}, 1000)
			  }
			  else
			  {
				self.orderId(returnedData);
				self.showDelivery(false);
				self.showThankyou(true);
                       	 	document.getElementById("step3").style.opacity = 0.7;
                        	document.getElementById("step4").style.opacity = 0;
			  }	
					
		});
		
		
	};

	

	this.getVendors = function()
	{
		
			
				$.get('/getVendors',
                    function(data)
					{
						data = JSON.parse(data);
						self.vendors(data);
					
						//self.vendors([{"vendor_name":"Anu Sweets","vendor_id":1,"start_time":"2014-02-09T10:00:00+05:30","end_time":"2014-08-09T22:00:00+05:30","vendor_house_number":"12","vendor_street":"Jawahar Nagar","vendor_city":"SGNR","item":"Rasgulla","item_id":1,"price":150,"type":"Veg","packing":"Polythene","cuisine":"Sweet"}]);

						self.vendors.sort(compare);	
						self.changeDataFormat();
                        self.findRestaurants();
					});
					
			
	}
	
	self.getCuisines = function()
	{
		$.get('/getCuisines',
			function(data)
			{
				data = JSON.parse(data);
				var cuisine = {};
				var cuisines =$.map(data , function(item){							
							cuisine.cuisine_name = item.cuisine_name;
							return new CuisineModel(cuisine);
								
						});
				self.cuisines(cuisines);	
			});
	}
	
        this.changeDataFormat = function()
	{
		var temp_vendor = {};
		temp_vendor.items = [];
		temp_vendor.vendor_cuisines = [];
		var vendor_id=0;
		var vendor_num = 0;		
		for(var i = 0; i< this.vendors().length; i++)
		{
			var vendor = this.vendors()[i];
			
			if(vendor_id == this.vendors()[i].vendor_id)
			{
				temp_vendor.items.push({name:vendor.item,packing:vendor.packing,price:vendor.price,id:vendor.item_id,cuisine:vendor.cuisine,type:vendor.type,vendor_id:vendor.vendor_id});	
			}
			else
			{
				vendor_num++;				
				if(vendor_id!=0)
				{
					var temp = new VendorModel(temp_vendor);
					self.vendorstemp.push(temp);					
				}
				vendor_id = vendor.vendor_id;
				temp_vendor={};
				temp_vendor.items = [];
				temp_vendor.vendor_cuisines = [];
				temp_vendor.vendor_id = vendor.vendor_id;
				temp_vendor.vendor_name = vendor.vendor_name;
				temp_vendor.vendor_city = vendor.vendor_city;
				temp_vendor.vendor_street = vendor.vendor_street;
				temp_vendor.vendor_house_number = vendor.vendor_house_number;
				temp_vendor.delivery_start = changeTimeFormat(vendor.start_time);
				temp_vendor.delivery_end = changeTimeFormat(vendor.end_time);
                                temp_vendor.status = getStatus(vendor.start_time,vendor.end_time);
				temp_vendor.items.push({name:vendor.item,packing:vendor.packing,price:vendor.price,id:vendor.item_id,cuisine:vendor.cuisine,type:vendor.type,vendor_id:vendor.vendor_id});
						
			}
	
			if(temp_vendor.vendor_cuisines.length ==0 || temp_vendor.vendor_cuisines.indexOf(vendor.cuisine)==-1)				
			{
				temp_vendor.vendor_cuisines.push({cuisine:vendor.cuisine});
			}		
		}
	
                self.vendorstemp.push(new VendorModel(temp_vendor));
	}

	
	this.findRestaurants = function()
	{
		
		for(var i = 0 ; i < self.vendorstemp().length ; i++)
		{	
			if(self.vendorstemp()[i].vendor_street == $("#location").val())
			{
				self.selectedVendors.push(self.vendorstemp()[i]);
			}
		}
                this.showVendor(true);
               // document.getElementById("step2").style.opacity = 0;
               // document.getElementById("step1").style.opacity = 0.7;
                
	}

	this.autofillForm = function()
	{
		var self = this;
		if($("#user").val()!='' && $("#user").val()!=null)
		{
			this.signedIn(true);
			$.get('/autofill',
				{username : $("#user").val()},
				function(data)
				{
					self.order().contactNum(data.contactNum);
					self.order().user(data.name);
					self.order().address(data.address);
				});
		}
		else
		{
			this.signedIn(false);
		}
	}
	
	this.init = function()
	{	
		this.getVendors();
	}
}

function compare( a , b )
{
    if(a.vendor_id < b.vendor_id)
    {
        return 1;
    }
    if(a.vendor_id > b.vendor_id)
    {
        return -1;
    }
    return 0;
}

function changeTimeFormat(time)
{
         var date = new Date(time);
	 var hours = date.getHours() == 0 ? "12" : date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
         var minutes = (date.getMinutes() < 9 ? "0" : "") + date.getMinutes();
         var ampm = date.getHours() < 12 ? "AM" : "PM";
         var formattedTime = hours + ":" + minutes + " " + ampm;
         return formattedTime;
}

function getStatus(start , end)
{
        //TODO: this still has some bugs.Need to expand it later!! 
        var starttime = new Date(start).toTimeString();
        var endtime = new Date(end).toTimeString();
        var time  = new Date().toTimeString();
        if(time >= starttime && time < endtime)
	{
		return "Open";
	}
	else
	{
		return "Closed For Now";
	}
}

$().ready(function(){

	vendorViewModel = new VendorViewModel();
         if ($('#vendor').get(0)) {
            ko.applyBindings(vendorViewModel, $('#vendor').get(0));
         }
         
        vendorViewModel.init();
});
