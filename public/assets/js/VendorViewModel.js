ko.bindingHandlers.jqSlider = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        //initialize the control
        var options = allBindingsAccessor().jqOptions || {};
        $(element).slider(options);


        //handle the value changing in the UI
        ko.utils.registerEventHandler(element, "slidechange", function() {
            //would need to do some more work here, if you want to bind against non-observables
            var observable = valueAccessor();

            observable($(element).slider("value"));
        });

    },
    //handle the model value changing
    update: function(element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).slider("value", value); 
        vendorViewModel.refineSearch();  

    }


};

var vendorViewModel;

function VendorViewModel()
{
	var self = this;
        this.vendors = ko.observableArray();
	this.vendorstemp = ko.observableArray();
	//this.city_array = ko.observableArray(["All","Delhi"]);
	//this.selected_city = ko.observable("All");
	this.priceLimit = ko.observable(1500);
        this.foodType = ko.observableArray();
	this.cuisines = ko.observableArray();
        this.vendorLocations = ko.observableArray();
        this.selectedLocation = ko.observable();
	this.checkedCuisine = ko.observableArray();  
	this.selectedVendors = ko.observableArray();
	this.allItems = ko.observableArray();
	this.filteredItems = ko.observableArray();
	this.step1 = ko.observable(true);
	this.step21 = ko.observable(false);
	this.step22 = ko.observable(false);
	this.orders = ko.observableArray();
	this.getVendors = function()
	{
		
			
				$.get('/getVendors',
                                       function(data)
					{
						data = JSON.parse(data);
						self.vendors(data);
						//self.vendors([{"vendor_name":"Bikanervala","vendor_id":1,"vendor_house_number":"","vendor_street":"Sector 63","vendor_city":"Noida","item":"Mini Thali(2 Roti,Panner Veg,Dal Makhani,Raita,Pic","item_id":1,"price":135,"type":"Veg","packing":"Thali","cuisine":"North Indian"},{"vendor_name":"Bikanervala","vendor_id":1,"vendor_house_number":"","vendor_street":"Sector 63","vendor_city":"Noida","item":"Mini Thali(2 Roti,Panner Veg,Dal Makhani,Raita,Pic","item_id":1,"price":135,"type":"Veg","packing":"Thali","cuisine":"Thali"},{"vendor_name":"Meeting Point Corner","vendor_id":2,"vendor_house_number":"","vendor_street":"Sector 62","vendor_city":"Noida","item":"Special Thali(2 Roti,Panner Veg,Dal Makhani,Raita,","item_id":2,"price":135,"type":"Veg","packing":"Thali","cuisine":"North Indian"},{"vendor_name":"Meeting Point Corner","vendor_id":2,"vendor_house_number":"","vendor_street":"Sector 62","vendor_city":"Noida","item":"Special Thali(2 Roti,Panner Veg,Dal Makhani,Raita,","item_id":2,"price":135,"type":"Veg","packing":"Thali","cuisine":"Thali"},{"vendor_name":"Essen Foods","vendor_id":4,"vendor_house_number":"","vendor_street":"Sector 65","vendor_city":"Noida","item":"Special Thali(2 Roti,Panner Veg,Dal Makhani,Raita,","item_id":3,"price":175,"type":"Veg","packing":"Thali","cuisine":"North Indian"},{"vendor_name":"Essen Foods","vendor_id":4,"vendor_house_number":"","vendor_street":"Sector 65","vendor_city":"Noida","item":"Special Thali(2 Roti,Panner Veg,Dal Makhani,Raita,","item_id":3,"price":175,"type":"Veg","packing":"Thali","cuisine":"Thali"}]);
						self.vendors.sort(compare);	
						self.changeDataFormat();
					});
			
	}
	
	self.getCuisines = function()
	{
		$.get('/getCuisines',
			function(data)
			{
				data = JSON.parse(data);
				var cuisine = {};
				var cuisines =$.map(data , function(item){							
							cuisine.cuisine_name = item.cuisine_name;
							return new CuisineModel(cuisine);
								
						});
				self.cuisines(cuisines);	
			});
	}
	
        this.changeDataFormat = function()
	{
		var temp_vendor = {};
		temp_vendor.items = [];
		temp_vendor.vendor_cuisines = [];
		var vendor_id=0;		
		for(var i = 0; i< this.vendors().length; i++)
		{
			var vendor = this.vendors()[i];
			if(vendor_id == this.vendors()[i].vendor_id)
			{
				temp_vendor.items.push({name:vendor.item,packing:vendor.packing,price:vendor.price,id:vendor.item_id,cuisine:vendor.cuisine,type:vendor.type,vendor_id:vendor.vendor_id});	
			}
			else
			{
				if(vendor_id!=0)
				{
					var temp = new VendorModel(temp_vendor);
					self.vendorstemp.push(temp);					
				}
				vendor_id = vendor.vendor_id;
				temp_vendor={};
				temp_vendor.items = [];
				temp_vendor.vendor_cuisines = [];
				temp_vendor.vendor_id = vendor.vendor_id;
				temp_vendor.vendor_name = vendor.vendor_name;
				temp_vendor.vendor_city = vendor.vendor_city;
				temp_vendor.vendor_street = vendor.vendor_street;
				temp_vendor.vendor_house_number = vendor.vendor_house_number;
				temp_vendor.vendor_delivery_start = '';
				temp_vendor.vendor_delivery_end = '';
				temp_vendor.items.push({name:vendor.item,packing:vendor.packing,price:vendor.price,id:vendor.item_id,cuisine:vendor.cuisine,type:vendor.type,vendor_id:vendor.vendor_id});
						
			}
			if(temp_vendor.vendor_cuisines.length ==0 || temp_vendor.vendor_cuisines.indexOf(vendor.cuisine)==-1)				
			{
				temp_vendor.vendor_cuisines.push({cuisine:vendor.cuisine});
			}		
		}
		this.getVendorLocations();
	}

	this.fillCityArray = function()
	{
		var city;
		for(i = 0; i < this.vendorstemp().length; i++)
		{
			city = this.vendorstemp()[i].vendor_city;

			if(this.city_array.indexOf(city) == -1)
			{			
				this.city_array.push(city);
			}
		}
	}
	
	this.getVendorLocations = function() 
	{
		for(var i =0 ; i < this.vendorstemp().length ; i++)
		{
			if(this.vendorLocations.indexOf(this.vendorstemp()[i].vendor_street)==-1)
			{
				this.vendorLocations.push({vendor_street : this.vendorstemp()[i].vendor_street});
			}
		}
	}

	this.findRestaurants = function()
	{
		this.step1(false);
		this.step21(true);
		for(var i = 0 ; i < self.vendorstemp().length ; i++)
		{	
			if(self.vendorstemp()[i].vendor_street == this.selectedLocation())
			{
				self.selectedVendors.push(self.vendorstemp()[i]);
			}
		}
	}

	
		
	
	this.refineSearch = function()
	{
			
		var temp_var, temp;
	
		if(self.filteredItems().length != 0)
		{
			self.filteredItems.removeAll();
		}

		if(self.checkedCuisine().length == 0 && self.foodType().length==0)
		{
			for(var i = 0; i< self.allItems().length; i++)
			{		
                                if(self.allItems()[i].price <= self.priceLimit())
				{	
					var itemTemp = new ItemModel(self.allItems()[i],1);
					self.filteredItems.push(itemTemp);
				}
			}
                        return;
		}
				
		for(var i = 0; i< self.checkedCuisine().length; i++)
		{
		
			temp_var = self.checkedCuisine()[i];
			for(var j = 0; j< self.allItems().length; j++)
			{
				if(temp_var == self.allItems()[j].cuisine)
				{
					if(self.filteredItems.indexOf(self.allItems()[j])==-1 && self.allItems()[j].price <= self.priceLimit())
					{	
						
						if(self.foodType().length == 0 || self.foodType().length == 2 || self.foodType.indexOf(self.allItems()[j].type) !=-1)			
						{
							var itemTemp = new ItemModel(self.allItems()[j],1);
							self.filteredItems.push(itemTemp);
						}
					}
				}
			}

		}
                if(self.checkedCuisine().length == 0)
                {
			for(var i = 0; i< self.foodType().length; i++)
			{
		
				temp_var = self.foodType()[i];
				for(var j = 0; j< self.allItems().length; j++)
				{
					if(temp_var == self.allItems()[j].type)
					{
						if(self.filteredItems.indexOf(self.allItems()[j])==-1 && self.allItems()[j].price <= self.priceLimit())
						{	
						
							
								var itemTemp = new ItemModel(self.allItems()[j],1);
								self.filteredItems.push(itemTemp);
							
						}
					}
				}

			}
		}
		return true;
		/*-temp = self.checkedCuisine()[i];
		if(self.selected_city() == "All" || self.selected_city() == temp.vendor_city)			
		{
			self.selectedVendors.push(temp);
		}*/
	}
	

	this.init = function()
	{
		this.getCuisines();		
		this.getVendors();
		this.fillCityArray();
	}
}

function compare( a , b )
{
    if(a.vendor_id < b.vendor_id)
    {
        return 1;
    }
    if(a.vendor_id > b.vendor_id)
    {
        return -1;
    }
    return 0;
}

$().ready(function(){

	vendorViewModel = new VendorViewModel();
	ko.applyBindings(vendorViewModel);
        vendorViewModel.init();
});
