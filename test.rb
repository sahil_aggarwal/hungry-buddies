require 'rubygems'
 require 'data_mapper'
 require 'sinatra'
 require 'haml'
 require 'json'
 #require 'pry'
 #require 'mail'
 #require 'pony'
enable :sessions

 DataMapper::Logger.new($stdout, :debug)
 				  
  DataMapper.setup(:default, 'postgres://eenbhcpjsziazr:Z1uAUffAbrmPgDikyH0oP8xc7p@ec2-54-197-251-18.compute-1.amazonaws.com/dfftqtk2iraovg')
 DataMapper::Property.auto_validation(false)
 DataMapper::Property.required(false)

class Feedback
	include DataMapper::Resource
	property :id,   Serial
	property :uname, String
	property :umessage, String
        property :time, Time
	property :email, String
end

class Vendor
	include DataMapper::Resource
	property :id,   Serial
	property :name, String
	property :house_number, String
        property :street, String
        property :city, String
        property :delivery_from, Time
        property :delivery_to, Time
        has n, :items
end

class Item
	include DataMapper::Resource
	property :id,   Serial
	property :name, String
	property :price, Integer
        property :type, String
        property :packing_type, String
        belongs_to :vendor
        has n, :orders, :through=> Resource
        has n, :cuisines, :through => Resource
end

class Cuisine
       include DataMapper::Resource
	property :id,   Serial
	property :cuisine_name, String
        has n, :items, :through => Resource
end

class User
  include DataMapper::Resource
  property :id, Serial
  property :name, String
  property :password, String
  property :emailId, String ,:key => true
  property :contactNum, String
  property :address, String
end

class Order
  include DataMapper::Resource
  property :id, Serial
  property :user ,String ,:required => false
  property :username , String
  property :time, String
  property :address, String
  property :status, Boolean, :default => false
  property :contactNumber, String
  has n, :items, :through => Resource
end

class ItemOrder
  include DataMapper::Resource
  property :quantity, Integer
  property :id, Serial
end

DataMapper.auto_upgrade!
DataMapper.finalize

def haml_partial(template, locals = {})
      haml(('partials/' + template.to_s).to_sym, :locals => locals)
end

$location ='';
get '/' do
  haml :index
end

get '/1' do
  User.create(:emailId => "admin", :password => "admin");
end

get '/contact' do
  haml:contact
end

get '/admin' do
  haml :admin 
end


get '/help' do
  haml :help
end

get '/users' do
 @users = User.all();
 @users.to_json;
end

#TODO change it to include admin credentials
get '/adminPage' do

@admin = User.all(:emailId => params[:username],:password => params[:password])
begin
       if @admin.length==0	
        raise "Invalid Username or Password"
       else
	session['admin'] = "adminLogin"	
        response = {:message =>"Success"}
       end
rescue => e
response = {:error => e.message}
end
content_type :json
response.to_json;
end

get '/showAdmin' do
 begin
   raise "Wrong username or password" if session['admin'].nil?
   haml :adminPage
 rescue => e
  response = "YOU ARE NOT LOGGED IN TO VIEW THIS PAGE"
 end
end



get '/showcart' do
 begin
   raise "Kindly Login To View Your Cart" if session['user'].nil? or session['user'] == ''
   haml :cart
 rescue => e
  response = "YOU ARE NOT LOGGED IN TO VIEW THIS PAGE"
 end
end

get '/cart' do
  if session['user'].nil? or session['user'] == ''
     response = {:error => "Kindly Login To View Your Cart"};
     content_type :json
     response.to_json
  else
    $user = session['user'];
  end
end

get '/getcart' do

   begin
    raise "Kindly Login To View Your Cart" if session['user'].nil? or session['user'] == ''
    response = repository(:default).adapter.select('select orders.username,vendors.name as vendor,items.name as item,items.price,item_orders.quantity from orders,item_orders,items,vendors where orders.id = item_orders.order_id and item_orders.item_id = items.id and vendors.id = items.vendor_id').map(&:to_h).to_json
   rescue => e
    response = "YOU ARE NOT LOGGED IN TO VIEW THIS PAGE"
   end
  
  
end

get '/getOrders' do
@orders = Order.all();
@orders.to_json
end

post '/orderItems' do
@param = JSON.parse request.body.read
@items = Array.new
@item = Object.new
@item_arr = Array.new
@orderItems = ItemOrder.all();
@orderItems.each do |orderItem|
	if orderItem.order_id == @param["id"]
        	@item = Item.first(:id => orderItem.item_id);              
		@itemHash =Hash["id" => @item.id,"name" => @item.name,"price" => @item.price, "packing_type" => @item.packing_type,"quantity" => orderItem.quantity,"type" => @item.type]
		@items << @itemHash
	end
end
@items.to_json

#Mail.deliver do
 #      to 'aggarwalsahil1992@gmail.com'
  #   from 'aggarwalsahil1992@gmail.com'
 # subject 'testing sendmail'
 #    body 'testing sendmail'
#end
end

get '/vendor/:location' do
  $location = params[:location];
end  



get '/getCuisines' do
	@cuisines = Cuisine.all();
        @cuisines.to_json;
end

get "/getVendors" do
@vendors = repository(:default).adapter.select('select vendors.name as Vendor_Name ,vendors.id as Vendor_Id,vendors.delivery_from as start_Time,vendors.delivery_to as End_Time,vendors.house_number as Vendor_House_Number,vendors.street as Vendor_Street,vendors.city as Vendor_City,items.name as item ,items.id as Item_Id,items.price as Price,items.type as Type,items.packing_type as Packing,cuisines.cuisine_name as Cuisine from vendors,items,cuisines,cuisine_items where items.id = cuisine_items.item_id and cuisine_items.cuisine_id = cuisines.id and vendors.id = items.vendor_id').map(&:to_h).to_json

end


get '/vendor' do
haml :vendor
end

get '/index' do
haml :index
end

get '/terms' do
haml :termsofuse
end
get '/allVendor' do
@vendor = Vendor.all();

#@vendor.items
#@vendor.items.name
@vendor.to_json	

end

post '/order' do
begin
@order = Order.create(:contactNumber => params[:contactNum],:user => params[:user],:address => params[:address],:username => params[:username],:time => Time.now)
 if @order == nil
  raise "cannot create order"
 else
  @items = params[:items];
  @items.each{|item| ItemOrder.create(:order_id => @order.id,:item_id => item[1][:item].to_i,:quantity => item[1][:quantity].to_i);}
  response = @order.id
end
rescue => e
response = {:error => e.message}
end
content_type :json
response.to_json
end

post '/vendor' do
@param = JSON.parse request.body.read
@vendor = Vendor.create(:name => @param["name"],:house_number => @param["house_number"],:street => @param["street"],:city => @param["city"],:delivery_from => @param["delivery_from"],:delivery_to => @param["delivery_to"])
@vendor.save
content_type :json
@vendor.to_json
end

get '/getVendorDetails' do
@vendor = Vendor.all();
@vendor.to_json
end

post '/deleteVendor' do
@param = JSON.parse request.body.read
@vendor = Vendor.get(@param["id"])
@vendor.destroy
end

post '/addItem' do
  @param = JSON.parse request.body.read
  @item = Item.create(:name =>@param["name"],:price =>@param["price"],:type =>@param["type"],:packing_type =>@param["packing_type"],:vendor_id =>@param["vendor_id"])
  @cuisine = Cuisine.first(:cuisine_name => @param["cuisine"])
if @cuisine.nil?
  @cuisine = Cuisine.create(:cuisine_name => @param["cuisine"])
end
  @zoo = CuisineItem.create(:item_id => @item.id, :cuisine_id => @cuisine.id)

end

post '/showItem' do
@param = JSON.parse request.body.read
@items = Item.all(:vendor_id => @param["id"]);
@items.to_json
end


post '/deleteItem' do
@param = JSON.parse request.body.read
@item = Item.get(@param["id"]);
@item.item_orders.all.destroy
@item.cuisine_items.all.destroy
@item.destroy
@items = Item.all();
@items.to_json
end

post '/setOrderStatus' do
@order = Order.get(params[:id])
@order.update(:status => true);	
end

post '/contact' do
@param = JSON.parse request.body.read
 Pony.mail({
:from => @param["name"],
    :to => 'aggarwalsahil1992@gmail.com',
    :subject => @param["name"] + "has contacted you via the Website",
    :body => @param["comment"],
    :via => :smtp,
    :via_options => {
     :address              => 'smtp.gmail.com',
     :port                 => '25',
     :enable_starttls_auto => true,
     :user_name            => 'aggarwalsahil1992@gmail.com',
     :password             => '**********',
     :authentication       => :plain, 
     :domain               => "localhost.localdomain" 
     }
    })
    
end

post '/user' do
@param = JSON.parse request.body.read
begin

  @user = User.first(:emailId => @param["emailId"])
  unless @user.nil?
    raise "Email ID already exists"
  end
    @user=User.create(:name => @param["name"],:password => @param["password"],:emailId => @param["emailId"], :address => @param["address"],:contactNum => @param["contactNum"]);
  session['user'] = @param["emailId"];
  session['userid'] = @user.id;
  $user = session['user'];
rescue => e
  response = {:error => e.message}
  content_type :json
  response.to_json;
end

end

post '/login' do 
 begin 
   @he =User.first(:emailId => params[:user_name], :password => params[:pass]); 
   raise "Invalid Username or Password" if @he.nil?
   response = @he;
   session['user']=params[:user_name];
   session['userid'] = @he.id;
   $user = session['user'];
   rescue => e
     response = {:error => {:message => e.message}}
   end
   content_type :json
   response.to_json
end

get '/signout' do
   session['user']='';
   session['userid'] = '';
   $user = session['user'];
   redirect('/');
end

get '/help' do
  haml :help
end


get '/viewProfile' do
 begin
 raise "You are not logged in" if session['user'].nil? or session['user'] == ''
 haml :profile
rescue => e
response = "YOU ARE NOT LOGGED IN TO VIEW THIS PAGE"
end

end

get '/profile' do
  if session['user'].nil? or session['user'] == ''
     response = {:error => "Not logged in"};
     content_type :json
     response.to_json
  else
    $user = session['user'];
  end
end

get '/getProfile' do
begin
   if session['userid'] == nil
     raise 'session id not found please login again'
   else	
     @userid = session['userid']; 
     @user_name = session['user'];  	 
     @userProfile = User.get(@userid.to_i,@user_name);
     if @userProfile == nil
       raise 'user profile doesn\'t exists';
     else
     response = @userProfile
     end
   end
rescue => e
 response = {:error => e.message}
end
content_type :json
response.to_json;
end

post '/updateProfile' do
begin
@user = User.get(params[:id],params[:emailId])
 if @user == nil
  raise 'user doesn\'t exists'
 else
  @updated = @user.update(:name => params[:name],:address => params[:address],:contactNum => params[:contactNum],:emailId => params[:emailId])
  if @updated == true
    response = {:message =>"update successful"}
  else
   raise 'update failed'
  end 
 end
rescue => e
 response = {:error => e.message}
end
content_type :json
response.to_json
end

get '/autofill' do
  @auto = User.first(:emailId => params[:username]);
  content_type :json
  @auto.to_json
end

get '/deleteOrder' do
begin
#binding.pry
  @order = Order.get(params[:id]);
  @item_order = ItemOrder.all(:order_id => params[:id]);
  if @order == nil || @item_order == nil
   raise "order not found in database"
  else		
   if @item_order.all.destroy == false ||@order.destroy  == false
     raise "could not delete order"	
   else
     response = {:message =>"Success"}
   end
  end
 rescue => e
  response = {:error => e.message}
end
content_type :json
response.to_json;
end
# To insert feedbacks
post '/addFeedback' do
begin
@feedback = Feedback.create(:uname => params[:uname], :umessage => params[:umessage], :email => params[:email],:time =>Time.now);
 if @feedback == nil
  raise "could not added feedback"
 else
  response = {:message =>"Success"}
 end
rescue => e
 response = {:error => e.message}
end
content_type :json
response.to_json;
end

# to retrieve feedbacks
get '/feedback' do
@feedbacks = Feedback.all();
@feedbacks.to_json
end

# to delete feedback
get '/deleteFeedback' do
begin
  @feedback = Feedback.get(params[:id]);
  if @feedback == nil
   raise "feedback not found in database"
  else		
   if @feedback.destroy == false
     raise "could not delete feedback"	
   else
     response = {:message =>"Success"}
   end
  end
 rescue => e
  response = {:error => e.message}
end
content_type :json
response.to_json;
end
